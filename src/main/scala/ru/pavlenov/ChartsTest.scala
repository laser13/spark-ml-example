package ru.pavlenov

import com.quantifind.charts.Highcharts._
import org.jfree.chart.{ChartFactory, ChartFrame}
import org.jfree.data.xy.DefaultXYDataset

/**
 * ⓭ + 26
 * Какой сам? by Pavlenov Semen 07.08.15.
 */
object ChartsTest extends App{

  areaspline(List(1, 2, 3, 4, 5), List(4, 1, 3, 2, 6))

  pie(Seq(4, 4, 5, 9))

  line((0 until 5).map(x => x -> x*x))
  line(Seq(0, 1, 4, 9, 16))
  def f(x: Int): Int = scala.math.pow(x, 2).toInt
  line(0 to 4, f _)

  regression((0 until 100).map(x => -x + scala.util.Random.nextInt(25)))

  histogram(Seq(1, 2, 2, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 6, 7), 7)

  val x = Array[Double](1,2,3,4,5,6,7,8,9,10)
  val y = x.map(_*2)
  val dataset = new DefaultXYDataset
  dataset.addSeries("Series 1",Array(x,y))

  val frame = new ChartFrame(
    "Title",
    ChartFactory.createScatterPlot(
      "Plot",
      "X Label",
      "Y Label",
      dataset,
      org.jfree.chart.plot.PlotOrientation.HORIZONTAL,
      false,false,false
    )
  )
  frame.pack()
  frame.setVisible(true)

}
