package ru.pavlenov

import org.apache.spark.SparkContext
import org.apache.spark.ml.evaluation.{RegressionEvaluator, BinaryClassificationEvaluator}
import org.apache.spark.ml.{PipelineModel, PipelineStage, Pipeline}
import org.apache.spark.ml.feature.{VectorAssembler, PolynomialExpansion, StandardScaler}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.regression.{LinearRegressionModel, LinearRegression}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.mllib.linalg
import org.apache.spark.mllib.linalg.{Vectors, DenseVector}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SQLContext}

import scala.collection.mutable

/**
 * ⓭ + 52
 * Какой сам? by Pavlenov Semen 06.08.15.
 */
object Main extends App {

  val sc = new SparkContext("local[*]", "YearPrediction")

  val rawData: RDD[(Double, linalg.Vector, linalg.Vector)] = sc.textFile("data/YearPredictionMSD.txt")
    .map(_.split(','))
    .map(x => (
      x.head.toDouble,
      Vectors.dense(x.take(12).map(_.toDouble)),
      Vectors.dense(x.takeRight(78).map(_.toDouble))
    ))

//  val labeledPointsRDD: RDD[LabeledPoint] = MLUtils.loadLabeledPoints(sc, "data/prepare")

  val sqlContext = new SQLContext(sc)
  import sqlContext.implicits.rddToDataFrameHolder

  val rawDF: DataFrame = rawData.toDF("label", "avg", "cov")

  val scalerAvg = new StandardScaler()
    .setWithMean(true)
    .setWithStd(true)
    .setInputCol("avg")
    .setOutputCol("features")
    .fit(rawDF)

  val polynomAvg = new PolynomialExpansion()
    .setInputCol("features")
    .setOutputCol("polyFeatures")
    .setDegree(2)

  val splitedData = rawDF.randomSplit(Array(0.8,0.2), 12345).map(_.cache())
  val trainData = splitedData(0)
  val testData = splitedData(1)

  val linReg = new LinearRegression()
    .setFeaturesCol("polyFeatures")
    .setLabelCol("label")

  val pipeline = new Pipeline().setStages(Array(
    scalerAvg,
    polynomAvg,
    linReg
  ))


  val paramGrid: Array[ParamMap] = new ParamGridBuilder()
//    .addGrid(linReg.maxIter, Array(5, 500))
//    .addGrid(linReg.regParam, Array(1e-15, 1e-10))
//    .addGrid(linReg.tol, Array(1e-9, 1e-6, 1e-3))
//    .addGrid(linReg.elasticNetParam, Array(0, 0.5, 1))
    .build()

  val crossval = new CrossValidator()
    .setEstimator(pipeline)
    .setEvaluator(new RegressionEvaluator)
    .setEstimatorParamMaps(paramGrid)
    .setNumFolds(3)

  // Train the model
  val startTime = System.nanoTime()

  val cvModel = crossval.fit(trainData)
//  val pipelineModel = pipeline.fit(trainData, paramGrid)
  val elapsedTime = (System.nanoTime() - startTime) / 1e9

  println()
  println("="*120)

//  pipelineModel.foreach(model => {
//    val fullPredictions = model.transform(testData).cache()
//    val predictions = fullPredictions.select("prediction").map(_.getDouble(0))
//    val labels = fullPredictions.select("label").map(_.getDouble(0))
//    val RMSE = new RegressionMetrics(predictions.zip(labels)).rootMeanSquaredError
//    val MAR = new RegressionMetrics(predictions.zip(labels)).meanAbsoluteError
//
//    val lrm = model.stages.last.asInstanceOf[LinearRegressionModel]
//
//    println("="*120)
//    println(s"iter: ${lrm.getMaxIter} tol: ${lrm.getTol} reg: ${lrm.getRegParam} elastic: ${lrm.getElasticNetParam}\nRMSE: $RMSE\nMAR: $MAR")
//  })
//
//  val (bestModel, rmseVal): (PipelineModel, Double) = pipelineModel.map(model => {
//    val fullPredictions = model.transform(valData)
//    val predictions = fullPredictions.select("prediction").map(_.getDouble(0))
//    val labels = fullPredictions.select("label").map(_.getDouble(0))
//    val RMSE = new RegressionMetrics(predictions.zip(labels)).rootMeanSquaredError
//    model -> RMSE
//  }).reduce((m1, m2) => if (m1._2 < m2._2) m1 else m2)


//  val fullPredictions = bestModel.transform(testData)
//  val predictions = fullPredictions.select("prediction").map(_.getDouble(0))
//  val labels = fullPredictions.select("label").map(_.getDouble(0))
//  val rmseTest = new RegressionMetrics(predictions.zip(labels)).rootMeanSquaredError
//
//  println(s"Best $rmseVal $rmseTest")

//  val bestModel = cvModel.bestModel
//
//  val fullPredictions = bestModel.transform(testData)
//  val predictions = fullPredictions.select("prediction").map(_.getDouble(0))
//  val labels = fullPredictions.select("label").map(_.getDouble(0))
//  val rmseTest = new RegressionMetrics(predictions.zip(labels)).rootMeanSquaredError
//
//  println(s"rmseTest: $rmseTest")

  println(s"Training time: $elapsedTime seconds")


  sc.stop()


}
