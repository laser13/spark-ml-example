package ru.pavlenov

import org.apache.spark.SparkContext
import org.apache.spark.ml.{PipelineModel, Pipeline}
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{Normalizer}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.regression.{LinearRegression}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.mllib.linalg
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SQLContext}

/**
 * ⓭ + 38
 * Какой сам? by Pavlenov Semen 10.08.15.
 */
object Version4 extends App{

  val f1 = "data/millionsong.txt"
  val f2 = "data/YearPredictionMSD.txt"

  val sc = new SparkContext("local[*]", "YearPrediction")

  val rawData: RDD[(Double, linalg.Vector, linalg.Vector)] = sc.textFile(f2)
    .map(_.split(','))
    .map(x => (
      x.head.toDouble,
      Vectors.dense(x.tail.take(12).map(_.toDouble)),
      Vectors.dense(x.takeRight(78).map(_.toDouble))))

  val sqlContext = new SQLContext(sc)
  import sqlContext.implicits._

  val rawDF: DataFrame = rawData.toDF("label", "avg", "cov")

  val normAvg: Normalizer = new Normalizer()
    .setP(2.0)
    .setInputCol("avg")
    .setOutputCol("features")

  val splitedData = rawDF.randomSplit(Array(0.8, 0.2), 42).map(_.cache())
  val trainData = splitedData(0)
  val testData = splitedData(1)

  val linReg = new LinearRegression()
    .setFeaturesCol("features")
    .setLabelCol("label")

  val pipeline = new Pipeline().setStages(Array(
    normAvg,
    linReg
  ))

  val paramGrid: Array[ParamMap] = new ParamGridBuilder()
    .addGrid(linReg.maxIter, Array(200))
    .addGrid(linReg.regParam, Array(1e-10))
    .addGrid(linReg.tol, Array(1e-6))
    .addGrid(linReg.elasticNetParam, Array(0.0))
    .build()

  val crossVal = new CrossValidator()
    .setEstimator(pipeline)
    .setEvaluator(new RegressionEvaluator)
    .setEstimatorParamMaps(paramGrid)
    .setNumFolds(3)

  // Train the model
  val startTime = System.nanoTime()
  val bestModel = crossVal.fit(trainData)
  val elapsedTime = (System.nanoTime() - startTime) / 1e9

  println()
  println("="*120)

  val fullPredictions = bestModel.transform(testData)
  val predictions = fullPredictions.select("prediction").map(_.getDouble(0))
  val labels = fullPredictions.select("label").map(_.getDouble(0))
  val rmseTest = new RegressionMetrics(predictions.zip(labels)).rootMeanSquaredError

  fullPredictions.select("label", "prediction")
    .map(x => x.getAs[Double](0) -> x.getAs[Double](1))
    .take(10).foreach(println)

  println(f"RMSE: $rmseTest%.6f")
  println(s"Training time: $elapsedTime seconds")

  sc.stop()

}
