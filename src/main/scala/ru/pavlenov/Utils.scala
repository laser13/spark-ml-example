package ru.pavlenov

import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SQLContext}

/**
 * ⓭ + 52
 * Какой сам? by Pavlenov Semen 07.08.15.
 */
object Utils extends App{

  val sc = new SparkContext("local[*]", "Utils")
  val sqlContext = new SQLContext(sc)
  import sqlContext.implicits._

//  sc.textFile("data/millionsong.txt")
//    .map(x => x.split(',').toList)
//    .map(x => s"${x.head}, ${x.tail.mkString(" ")}")
//    .coalesce(1)
//    .saveAsTextFile("data/small")
//
//  sc.textFile("data/YearPredictionMSD.txt")
//    .map(x => x.split(',').toList)
//    .map(x => s"${x.head}, ${x.tail.mkString(" ")}")
//    .coalesce(8)
//    .saveAsTextFile("data/prepare")

  val rawData = sc.textFile("data/YearPredictionMSD.txt")
    .map(_.split(','))
    .map(x => x.head.toDouble)

  println(rawData.count())

  MLUtils.kFold(rawData, 3, 42)
    .map(x => x._1.count() -> x._2.count())
    .foreach(println)

}
