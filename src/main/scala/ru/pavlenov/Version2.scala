package ru.pavlenov

import org.apache.spark.SparkContext
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{Normalizer, PolynomialExpansion, StandardScaler, StandardScalerModel}
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.mllib.linalg
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SQLContext}

/**
 * ⓭ + 38
 * Какой сам? by Pavlenov Semen 10.08.15.
 */
object Version2 extends App{

  val f1 = "data/millionsong.txt"
  val f2 = "data/YearPredictionMSD.txt"

  val sc = new SparkContext("local[*]", "YearPrediction")

  val rawData: RDD[(Double, linalg.Vector, linalg.Vector)] = sc.textFile(f2)
    .map(_.split(','))
    .map(x => (
      x.head.toDouble,
      Vectors.dense(x.tail.take(12).map(_.toDouble)),
      Vectors.dense(x.takeRight(78).map(_.toDouble))))

  val sqlContext = new SQLContext(sc)
  import sqlContext.implicits._

  val rawDF: DataFrame = rawData.toDF("label", "avg", "cov")

  val normAvg: Normalizer = new Normalizer()
    .setP(2.0)
    .setInputCol("avg")
    .setOutputCol("features")

  val polynomAvg = new PolynomialExpansion()
    .setInputCol("features")
    .setOutputCol("polyFeatures")
    .setDegree(3)

  val splitedData = rawDF.randomSplit(Array(0.8, 0.2), 42).map(_.cache())
  val trainData = splitedData(0)
  val testData = splitedData(1)

  val linReg = new LinearRegression()
    .setFeaturesCol("polyFeatures")
    .setLabelCol("label")
    .setElasticNetParam(0.5)
    .setMaxIter(500)
    .setRegParam(1e-10)
    .setTol(1e-6)

  linReg.setFeaturesCol("polyFeatures")

  val pipeline = new Pipeline().setStages(Array(
    normAvg,
    polynomAvg,
    linReg
  ))

  // Train the model
  val startTime = System.nanoTime()
  val pipelineModel = pipeline.fit(trainData)
  val elapsedTime = (System.nanoTime() - startTime) / 1e9

  println()
  println("="*120)

  val fullPredictions = pipelineModel.transform(testData)
  val predictions = fullPredictions.select("prediction").map(_.getDouble(0))
  val labels = fullPredictions.select("label").map(_.getDouble(0))
  val rmseTest = new RegressionMetrics(predictions.zip(labels)).rootMeanSquaredError

  fullPredictions.select("label", "prediction")
    .map(x => x.getAs[Double](0) -> x.getAs[Double](1))
    .take(10).foreach(println)

  println(f"RMSE: $rmseTest%.6f")
  println(s"Training time: $elapsedTime seconds")

  sc.stop()

}
