package ru.pavlenov

import org.apache.spark.SparkContext
import org.apache.spark.ml.feature._
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.sql.{DataFrame, SQLContext}

/**
 * ⓭ + 16
 * Какой сам? by Pavlenov Semen 07.08.15.
 */
object Test extends App {

  val sc = new SparkContext("local[*]", "YearPrediction")
  val sqlContext = new SQLContext(sc)
  import sqlContext.implicits._

  val dataScaler = Array(
    Vectors.dense(1.0, 10.0, 1.0),
    Vectors.dense(2.0, 20.0, 50.0),
    Vectors.dense(3.0, 30.0, 100.0)
  )
  val dfScaler = sqlContext.createDataFrame(dataScaler.map(Tuple1.apply)).toDF("avg")
  val scalerAvg: StandardScalerModel = new StandardScaler()
    .setWithMean(true)
    .setWithStd(false)
    .setInputCol("avg")
    .setOutputCol("features")
    .fit(dfScaler)

  scalerAvg.transform(dfScaler).collect().foreach(println)

  val sentenceDataFrame = sqlContext.createDataFrame(Seq(
    (0, "Hi I heard about Spark"),
    (0, "I wish Java could use case classes"),
    (1, "Logistic regression models are neat neat")
  )).toDF("label", "sentence")
  val tokenizer = new Tokenizer().setInputCol("sentence").setOutputCol("words")
  val wordsDataFrame = tokenizer.transform(sentenceDataFrame)
  wordsDataFrame.select("words", "label").take(3).foreach(println)

  println("="*120)
  println("Binarizer")
  val dataBin = Array(
    (0, 0.1),
    (1, 0.8),
    (2, 0.2)
  )
  val dataFrame: DataFrame = sqlContext.createDataFrame(dataBin).toDF("label", "feature")

  val binarizer: Binarizer = new Binarizer()
    .setInputCol("feature")
    .setOutputCol("binarized_feature")
    .setThreshold(0.5)

  val binarizedDataFrame = binarizer.transform(dataFrame)
  val binarizedFeatures = binarizedDataFrame.select("binarized_feature")
  binarizedFeatures.collect().foreach(println)

  //=========================================================================================
  println("="*120)
  println("PolynomialExpansion")
  val dataPol = Array(
    Vectors.dense(1.0, 2.0),
    Vectors.dense(0.0, 0.0),
    Vectors.dense(2.0, 3.0)
  )
  val dfPol = sqlContext.createDataFrame(dataPol.map(Tuple1.apply)).toDF("features")
  val polynomialExpansion = new PolynomialExpansion()
    .setInputCol("features")
    .setOutputCol("polyFeatures")
    .setDegree(3)
  val polyDF = polynomialExpansion.transform(dfPol)
  polyDF.select("polyFeatures").take(3).foreach(println)

  //=========================================================================================
  println("="*120)
  println("StringIndexer")
  val dfSI = sqlContext.createDataFrame(
    Seq((0, "a"), (1, "b"), (2, "c"), (3, "a"), (4, "a"), (5, "c"))
  ).toDF("id", "category")
  val indexer = new StringIndexer()
    .setInputCol("category")
    .setOutputCol("categoryIndex")
  val indexed = indexer.fit(dfSI).transform(dfSI)
  indexed.show()

  //=========================================================================================
  println("="*120)
  println("OneHotEncoder")
  val encoder = new OneHotEncoder().setInputCol("categoryIndex").
    setOutputCol("categoryVec")
  val encoded = encoder.transform(indexed)
  encoded.select("id", "categoryVec").foreach(println)

}
