name := "year-prediction"

version := "1.0"

scalaVersion := "2.11.7"

val sparkVersion = "1.4.1"

libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion

libraryDependencies += "org.apache.spark" %% "spark-mllib" % sparkVersion

libraryDependencies += "com.github.wookietreiber" %% "scala-chart" % "latest.integration"

libraryDependencies += "com.itextpdf" % "itextpdf" % "5.5.6"

libraryDependencies += "org.jfree" % "jfreesvg" % "3.0"

libraryDependencies += "com.quantifind" %% "wisp" % "0.0.4"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }